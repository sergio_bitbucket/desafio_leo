const browserSync = require('browser-sync');
var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var ts = require('gulp-typescript');

gulp.task('compilar-html', function() {
    return gulp.src('./src/pages/*.html')
        .pipe(gulp.dest('./public'));
});

gulp.task('compilar-php', function() {
    return gulp.src('./src/pages/*.php')
        .pipe(gulp.dest('./public'));
});

gulp.task('compilar-images', function() {
    return gulp.src('./src/images/*')
        .pipe(gulp.dest('./public/images'));
});

gulp.task('compilar-sass', function () {
    
    return gulp.src([
        './src/*.scss', './src/scss/*.scss'
    ]).pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./public/css'));

});

gulp.task('compilar-js', function () {
    return gulp.src('./src/js/*.js')
            .pipe(uglify())
            .pipe(gulp.dest('./public/js'));
});

gulp.task('compilar-ts', function () {
    return gulp.src('./src/ts/*.ts')
            .pipe(ts({
                noImplicitAny: true,
                outFile: 'main.js'
            }))
            .pipe(uglify())
            .pipe(gulp.dest('./public/js'));
});

gulp.task('monitorar', function () {
    
    gulp.watch(
        ['./src/*.scss', './src/scss/*.scss'],
        gulp.series('compilar-sass')
    );
    gulp.watch(['src/*']).on('change', browserSync.reload);

    
    gulp.watch(
        ['./src/*.js', './src/js/*.js'],
        gulp.series('compilar-js')
    );

    gulp.watch(
        ['./src/*.ts', './src/ts/*.ts'],
        gulp.series('compilar-ts')
    );
    
    
    gulp.watch(
        ['./src/pages/*.html'],
        gulp.series('compilar-html')
    );

    gulp.watch(
        ['./src/pages/*.php'],
        gulp.series('compilar-php')
    );

    gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('default', gulp.series('monitorar'));
gulp.task('dev', gulp.series(['compilar-html', 'compilar-php', 'compilar-images',  'compilar-sass',  'compilar-js',  'compilar-ts', 'monitorar']));