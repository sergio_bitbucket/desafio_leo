<?php
/** 
* Esta é a classe Fachada, ela tem o objetivo de ligar as views ao controller.
* 
* @author Sergio Severino da Silva <elderseriogio.net@hotmail.com> 
* @version 0.1 
*/
require_once "../php/Controller/cursoController.php";

class Fachada {

    // ######################################## Cursos ########################################

    public function buscarCursos(){
        $usuario = new CursoController();
        return $usuario->buscarCursos();
    }

    public function criarCurso($curso){
        $usuario = new CursoController();
        $usuario->criarCurso($curso);
    }

    public function editarCurso($curso){
        $usuario = new CursoController();
        $usuario->editarCurso($curso);
    }

    public function deletarCurso($id_curso){
        $usuario = new CursoController();
        $usuario->deletarCurso($id_curso);
    }

}