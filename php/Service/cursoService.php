<?php
/** 
* Esta é a classe CursoService, ela tem o objetivo de detratar os dados antes de mandar para o banco de dados 
* como também  trata os dados vindo do banco antes de retornar para o controller.
* 
* @author Sergio Severino da Silva <elderseriogio.net@hotmail.com> 
* @version 0.1 
*/
require_once "../php/Repository/cursoRepository.php";

class CursoService {

    private $con;

    public function __construct() {
        $this->con = new CursoRepository();
    }

    public function buscarCursos(){
        return $this->con->buscarCursos();
    }

    public function criarCurso($curso){
        $this->con->criarCurso($curso);
    }

    public function editarCurso($curso){
        $this->con->editarCurso($curso);
    }

    public function deletarCurso($id_curso){
        $this->con->deletarCurso($id_curso);
    }


}
