<?php
/** 
* Esta é a classe CursoRepository, ela tem o objetivo de consatruir todos as querys para as conexões com o banco.
* 
* @author Sergio Severino da Silva <elderseriogio.net@hotmail.com> 
* @version 0.1 
*/
require_once "../php/Repository/conexaoRepository.php";

class CursoRepository {

    private $con;

    public function __construct() {
        $this->con = (new ConexaoRepository())->abrirConexao();
    }

    public function buscarCursos(){
        
        $sql = "SELECT 
                    *
                FROM cursos";
        $stmt = $this->con->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function criarCurso($curso){

        $sql = "INSERT INTO 
                    cursos
                (imagens, titulo, descricao, link_botao)
                VALUES
                (:imagens, :titulo, :descricao, :link_botao)";
        $stmt = $this->con->prepare($sql);
        $stmt->bindParam(":imagens", $curso["imagens"]);
        $stmt->bindParam(":titulo", $curso["titulo"]);
        $stmt->bindParam(":descricao", $curso["descricao"]);
        $stmt->bindParam(":link_botao", $curso["link_botao"]);
        $stmt->execute();

    }

    public function editarCurso($curso){

        $sql = "UPDATE 
                    cursos 
                SET 
                imagens = :imagens, 
                titulo= :titulo, 
                descricao = :descricao, 
                link_botao = :link_botao 
                WHERE id_curso = :id_curso";
         $stmt = $this->con->prepare($sql);
         $stmt->bindParam(":id_curso", $curso["id_curso"]);
         $stmt->bindParam(":imagens", $curso["imagens"]);
         $stmt->bindParam(":titulo", $curso["titulo"]);
         $stmt->bindParam(":descricao", $curso["descricao"]);
         $stmt->bindParam(":link_botao", $curso["link_botao"]);
         $stmt->execute();

    }

    public function deletarCurso($id_curso){

        $sql = "DELETE FROM cursos WHERE id_curso = :id_curso";
        $stmt = $this->con->prepare($sql);
        $stmt->bindParam(":id_curso", $id_curso);
        $stmt->execute();

    }
    
}
