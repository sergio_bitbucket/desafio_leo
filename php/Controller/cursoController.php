<?php
/** 
* Esta é a classe CursoController, ela tem o objetivo de ligar as Fachada ao Service.
* 
* @author Sergio Severino da Silva <elderseriogio.net@hotmail.com> 
* @version 0.1 
*/
require_once "../php/Service/cursoService.php";

class CursoController {

    private $con;

    public function __construct() {
        $this->con = new CursoService();
    }

    public function buscarCursos(){
        return $this->con->buscarCursos();
    }

    public function criarCurso($curso){
        $this->con->criarCurso($curso);
    }

    public function editarCurso($curso){
        $this->con->editarCurso($curso);
    }

    public function deletarCurso($id_curso){
        $this->con->deletarCurso($id_curso);
    }


}