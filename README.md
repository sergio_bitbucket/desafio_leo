**DADOS DO DESENVOLVEDOR**

**Nome:** Sergio Severino Da Silva

**Fone:** (81) 99758-4694

**E-mail:** eldersergio.net@hotmail.com

**Cargo:** Desenvolvedor Web Back-End

**DADOS DO PROJETO** 

**Nome:** desafio_leo

**PASTAS DO PROJETO**

**php:** Pasta onde esta todos os arquivos de php e o CRUD pedido para o back-end

**src:** Pasta onde esta todo o desenvovimento em html, scss e ts, bem deixei uma pasta em js e css mas preferir não usar.

**ARQUIVOS DO PROJETO** 

1. gulpfile.js 
2. pakage.json
3. tsconfig.json

**PASSOS PARA RODAR O PROJETO APÓS O GIT CLONE** 

**Back-End** 

1. Deve um servidor que no meu caso eu utilizei o Xampp.

2. Deve importar o arquivo "desafio_leo.sql" que etará dentro da pasta sql.

3. Deve por todas as configurações de acesso ao banco no arquivo "conexaoRepository.php" que esta dentro do caminho "php/Ropository/"

**Front-End** 

1. Presumo que você já tenha o node.js instalado em sua maquina e utilize o "npm"

2. Se não tiver o Gulp instalado deve usar o comando para instalar: "npm install gulp -g"

3. Deve instalar as dependências que estão no "package.json" com o comando: "npm isntall"

4. Após a página node_modules ser criada, agora é só fazer o comando "npm start"

5. Atenção após o comando "npm start" você verá que será criada uma pasta chamada "public" nela é onde rodará todo o projeto, sendo assim acessei desta forma no meu local "localhost/desafio_leo/public"