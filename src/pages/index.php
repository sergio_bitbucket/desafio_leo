<?php
/** 
* Esta é a pagina inicial do Projeto.* 
*
* @author Sergio Severino da Silva <elderseriogio.net@hotmail.com> 
* @version 0.1  
*/

//Bem eu decidir usar o require_once por achar que o projeto era pequeno ai por isso não nemespase e use. 
require_once "../php/Fachada/fachada.php";
$fachada = new Fachada();
$cursos = $fachada->buscarCursos();

/*Aqui estou certificando que o Cookie não existe para poder criar um novo,
* para poder controlar o primeiro acesso do usuário ao sistema.
*/
if(!isset($_COOKIE["acesso"])){
    setcookie("acesso", true, time() + (24*3600));
}

/*Aqui eu utilizei um formulário normal para testar
* a criação a edição e o delete do CRUD e a listagem ficou na lina 12
*/
if(isset($_POST["criar_curso"])){
    $fachada->criarCurso($_POST);
}

if(isset($_POST["editar_curso"])){
    $fachada->editarCurso($_POST);
}

if(isset($_POST["deletar_curso"])){
    $fachada->deletarCurso($_POST["id_curso"]);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LEO</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css" />
</head>
<body class="leo-layout">
<?php if(!isset($_COOKIE["acesso"])){ ?>
            <div id="dv_modal" class="modal">
                <div class="modal-content">
                    <div id="fechar_modal" class="fechar">
                    <i class="fa fa-times" aria-hidden="true"></i>
                    </div>
                    <div class="modal-img">
                        <img src="images/img_modal.jpg" alt="">
                    </div>
                    <div class="modal-body">
                        <h3>EGESTAS TORTOR VULPUTATE</h3>
                        <p>
                        Aenean eu leo quam. Pellentesque omare sem lacinia quam venenalis vestibulum. 
                        Donec ullamccrper nulla non metus auctor fringilla. Donec sed odio dui. Cras
                        </p>
                        <button>INSCREVA-SE</button>
                    </div>
                </div>
            </div>
        <?php } ?>
    <main>
        <header>
            <nav class="menu-topo">
                <a href="#">
                    <img src="images/leo.png" alt="">
                </a>
                <div class="menu-topo-lateral">
                    <form action="#" method="get" class="form-busca">
                        <div class="busca">
                            <input type="text" name="busca" placeholder="Pesquisar Cursos..." />
                            <span class="material-icons">search</span>
                        </div>
                    </form>
                    <div class="dropdown-container">
                        <div id="dropdown" class="dropdown">
                            <div class="dropdown-handler">
                                <div class="dropdown-image">
                                    <img src="images/foto.jpg" alt="" />
                                </div>
                                <div class="dropdown-info">
                                    <span class="welcome">Seja bem-vindo</span>
                                    <span class="nome">John Doe</span>
                                </div>
                                <span class="dropdown-icon material-icons">
                                    arrow_drop_down
                                </span>
                            </div>
                            <div class="dropdown-submenu">
                                <ul>
                                    <li><a href="#">Exemplo de menu</a></li>
                                    <li><a href="#">Exemplo de menu</a></li>
                                    <li><a href="#">Exemplo de menu</a></li>
                                    <li><a href="#">Exemplo de menu</a></li>
                                    <li><a href="#">Exemplo de menu</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </nav>
        </header>
        <section id="slider-show" class="slider-show">
            <div class="slider-show-container">
                <div id="ss-prev" class="slider-show-prev">
                    <span class="material-icons">navigate_before</span>
                </div>
                <div class="slider-show-content">
                    <?php 
                    $qtd_maxima_slide = 3;
                        for($i = 0; $i < $qtd_maxima_slide; $i++){ 
                    ?>
                            <div class="slider-show-step" data-imagem='<?php echo $cursos[$i]["imagens"]; ?>'>
                                <div class="slider-show-box">
                                    <h1><?php echo $cursos[$i]["titulo"]; ?></h1>
                                    <p><?php echo $cursos[$i]["descricao"]; ?></p>
                                    <a href='<?php echo $cursos[$i]["link_botao"] . "?id_curso=" . $cursos[$i]["id_curso"]; ?>' target="_blank" rel="noopener noreferrer">Ver Curso</a>
                                </div>
                            </div>
                    <?php 
                        } 
                    ?>
                </div>
                <div id="ss-next" class="slider-show-next">
                    <span class="material-icons">navigate_next</span>
                </div>
            </div>
            <div class="slider-show-footer">
                <div class="slider-show-steps"></div>
            </div>
        </section>
        <section class="courses">
            <h2 class="h2">Meus Cursos</h2>
            <div class="my-courses">
                <?php 
                    foreach($cursos as $curso) {
                ?>
                        <article class="course">
                            <div class="hero">
                                <img src="<?php echo $curso["imagens"]; ?>" alt="" />
                            </div>
                            <div class="info-box">
                                <h3><?php echo $curso["titulo"]; ?></h3>
                                <p><?php echo $curso["descricao"]; ?></p>
                                <a href='<?php echo $curso["link_botao"] . "?id_curso=" . $curso["id_curso"]; ?>'>Ver Curso</a>
                            </div>
                        </article>
                <?php } ?>
                <article class="add-course">
                    <img src="images/add-curso.png" alt="" />
                    <span class="menor">Adicionar</span>
                    <span class="maior">Curso</span>
                </article>
            </div>
        </section>
        <footer>
            <div class="rodape">
                <div class="scope-big">
                    <img src="images/leo-footer.png" alt="LEO" />
                    <p>Maecenas faucibus mollis interdum. Morbi leo risus, porta ac cpmsectetir ac, vestibulum at eros</p>
                </div>
                <div class="scope-small">
                    <h3>// CONTATO</h3>
                    <p>(21) 98765-3434</p>
                    <p>contato@leolearning.com</p>
                </div>
                <div class="scope-small">
                    <h3>// REDES SOCIAIS</h3>
                    <div class="social-icons">
                        <a href="#" target="_blank" rel="noopener noreferrer">
                            <object data="images/tw.svg" width="20" height="20"></object>
                        </a>
                        <a href="#" target="_blank" rel="noopener noreferrer">
                            <object data="images/yt.svg" width="20" height="20"></object>
                        </a>
                        <a href="#" target="_blank" rel="noopener noreferrer">
                            <object data="images/pt.svg" width="20" height="20"></object>
                        </a>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <span>Copyright 2017 - All right reserved.</span>
            </div>
        </footer>
    </main> 
    <script type="text/javascript" src="js/main.js"></script>
</body>
</html>