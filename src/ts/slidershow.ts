/**
 * Este é o arquivo onde esta todo o TS que é responsável pelo o slideshow, abrir e fechar o dropdown e fechar o modal
 * 
 * @author Sergio Severino da Silva <elderseriogio.net@hotmail.com>  
 * @version 0.1  
 */
class Slideshow {

    private intervalo: number;

    public initSlideShow() {
        const steps: HTMLCollection = document.getElementsByClassName('slider-show-step');
        const container: Element = document.getElementsByClassName('slider-show')[0];
        const points: Element = document.getElementsByClassName('slider-show-steps')[0];

        for (let index = 0; index < steps.length; index++) {
            const input: any = steps.item(index);
            const imagem = input.dataset.imagem;

            input.style.width = container.clientWidth + 'px';
            input.style.backgroundImage = "url(" + imagem + ")";

            const div: Element = document.createElement('div');
            div.classList.add('slide-show-point');

            if (index === 0) {
                div.classList.add('ativo');
            }
            points.append(div);
        }
        
        document.getElementById('ss-prev').addEventListener('click', (evt) => {
            evt.preventDefault();
            this.executeSlideShow(false);
        });
        
        document.getElementById('ss-next').addEventListener('click', (evt) => {
            evt.preventDefault();
            this.executeSlideShow();
        });        
        

    }

    public run(seconds: number) {
        this.intervalo = setInterval(this.executeSlideShow, seconds * 1000);
       
        // document.getElementById('slider-show').addEventListener('mouseover', () => {
        //     clearInterval(this.intervalo);
        // });
        // document.getElementById('slider-show').addEventListener('mouseout', () => {
        //     this.run(seconds);
        // });
    }

    public executeSlideShow(is_next: boolean = true) {
        
        const content: HTMLCollectionOf<Element> = document.getElementsByClassName('slider-show-step');
        const ativo: HTMLCollectionOf<Element> = document.getElementsByClassName('slider-show-step ativo');

        const step = document.getElementsByClassName('slide-show-point');
        const step_atual = document.getElementsByClassName('slide-show-point ativo');
        if (ativo.length > 0) {
            let indiceAtivo = 0;
            
            for (let index = 0; index < content.length; index++) {
                const input: Element = content.item(index);
                if (input.classList.contains('ativo')) {
                    indiceAtivo = index;
                }
            }

            content.item(indiceAtivo).classList.remove('ativo');
            step[indiceAtivo].classList.remove('ativo');
            let prox;
            if (is_next) {
                prox = indiceAtivo + 1;
            } else {
                if (indiceAtivo === 0) {
                    prox = content.length - 1;
                } else {
                    prox = indiceAtivo - 1;
                }
            }

            if (prox < content.length) {
                content.item(prox).classList.add('ativo');
                step[prox].classList.add('ativo');
            } else {
                content.item(0).classList.add('ativo');
                step[0].classList.add('ativo');
            }
        } else {
            content.item(0).classList.add('ativo');
            step_atual[0].classList.remove('ativo');
            step[0].classList.add('ativo');
        }
    }
}

const ss = new Slideshow();  
ss.initSlideShow();
ss.executeSlideShow();
ss.run(5);

document.getElementById('dropdown').addEventListener('click', (evt: MouseEvent) => {
    evt.preventDefault();
    const dropdown: HTMLElement = document.getElementById('dropdown');
    if (dropdown.classList.contains('active')) {
        dropdown.classList.remove('active');
    } else {
        dropdown.classList.add('active');
    }
});

window.onload = function(){
    var modal = document.getElementById('dv_modal');
    console.log(modal);
    if(modal){
        var fechar_modal = document.getElementById('fechar_modal');
        fechar_modal.onclick = function(){
            modal.style.display = 'none';
        }
    }
}

